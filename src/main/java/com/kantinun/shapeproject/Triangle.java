/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.shapeproject;

/**
 *
 * @author EAK
 */
public class Triangle {
    private double b,h;
    public Triangle(double b, double h){
        this.b = b;
        this.h = h;
    }
    public double calArea(){
        return 0.5*b*h;
    }
    public void setBH(double b, double h){
        if(b <= 0 || h <= 0){
            System.out.println("Error: Length must be more than zero");
            return;
        }
        this.b = b;
        this.h = h;
    }
    public String getBH(){
        return "B = " + this.b + " H = " + this.h;
    }
}

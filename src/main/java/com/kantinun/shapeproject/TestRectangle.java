/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.shapeproject;

/**
 *
 * @author EAK
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(2,4);
        System.out.println("Area of Rectangle(" + rectangle1.getWH() +") = " + rectangle1.calArea());
        rectangle1.setWH(3,3);
        System.out.println("Area of Rectangle(" + rectangle1.getWH() +") = " + rectangle1.calArea());
        rectangle1.setWH(0,0);
        System.out.println("Area of Rectangle(" + rectangle1.getWH() +") = " + rectangle1.calArea());
        rectangle1.setWH(5,3);
        System.out.println("Area of Rectangle(" + rectangle1.getWH() +") = " + rectangle1.calArea());
    }
}

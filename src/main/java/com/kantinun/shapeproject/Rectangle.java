/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.shapeproject;

/**
 *
 * @author EAK
 */
public class Rectangle {
    private double w,h;
    public Rectangle(double w, double h){
        this.w = w;
        this.h = h;
    }
    public double calArea(){
        return w*h;
    }
    public void setWH(double w, double h){
        if(w <= 0 || h <= 0){
            System.out.println("Error: Length must be more than zero");
            return;
        }
        else if(w == h){
            System.out.println("Error: Width must not equal Height");
            return;
        }
        this.w = w;
        this.h = h;
    }
    public String getWH(){
        return "W = " + this.w + " H = " + this.h;
    }
    
}
